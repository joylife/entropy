(function () {
    "use strict";
    var assert = require("chai").assert;

    describe("entropy", function () {
        describe("guid", function () {
            var guid = require("./").guid;

            it("is a function", function (callback) {
                assert.isFunction(guid);
                return callback();
            });

            it("generates a string", function (callback) {
                assert.isString(guid());
                return callback();
            });

            it("is different everytime", function (callback) {
                assert.notEqual(guid(), guid());
                return callback();
            });
        });

        describe("uniqueIdBuffer", function () {
            var uniqueId = require("./").uniqueIdBuffer;
            it("generate a unique id", function () {
                var uid = uniqueId();
                assert(uid);
                assert(require("buffer").Buffer.isBuffer(uid));
                assert.notStrictEqual(uid.toString("base64"), (uid = uniqueId()).toString("base64"));
                assert.notStrictEqual(uid.toString("base64"), uniqueId().toString("base64"));
            });
        });


        describe("uniqueIdBase64", function () {
            var uniqueId = require("./").uniqueIdBase64;
            it("generate a unique id", function () {
                var uid = uniqueId();
                assert(uid);
                assert.strictEqual(typeof uid, "string");
                assert.notStrictEqual(uid, (uid = uniqueId()));
                assert.notStrictEqual(uid, uniqueId());
            });
        });


        describe("uniqueIdHex", function () {
            var uniqueId = require("./").uniqueIdHex;
            it("generate a unique id", function () {
                var uid = uniqueId();
                assert(uid);
                assert.strictEqual(typeof uid, "string");
                assert.notStrictEqual(uid, (uid = uniqueId()));
                assert.notStrictEqual(uid, uniqueId());
            });
        });

        describe("base64ToHex/hexToBase64", function () {
            var entropy = require("./");
            var b = entropy.uniqueIdBase64;
            var h = entropy.uniqueIdHex;
            var hfromb = entropy.base64ToHex;
            var bfromh = entropy.hexToBase64;
            it("converts base64 ids to hex", function () {
                var base64 = b();
                var hex = h();
                assert.strictEqual(base64, bfromh(hfromb(base64)));
                assert.strictEqual(hex, hfromb(bfromh(hex)));
            });
        });
    });
})();

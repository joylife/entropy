(function (exports) {

    "use strict";

    var crypto = require("crypto");
    var Cuid = require("cuid");

    var guid = function () {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c === "x" ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    };

    var cuid = function () {
        return Cuid();
    };

    var uniqueIdBuffer = function () {
        return crypto.randomBytes(24);
    };

    var uniqueIdBase64 = function () {
        return crypto.randomBytes(24).toString("base64");
    };

    var uniqueIdHex = function () {
        return crypto.randomBytes(24).toString("hex");
    };

    var hexToBase64 = function (hex) {
        return (new Buffer(hex, "hex")).toString("base64");
    };

    var base64ToHex = function (base64) {
        return (new Buffer(base64, "base64")).toString("hex");
    };

    exports.guid = guid;
    exports.cuid = cuid;
    exports.uniqueIdBuffer = uniqueIdBuffer;
    exports.uniqueIdBase64 = uniqueIdBase64;
    exports.uniqueIdHex = uniqueIdHex;
    exports.hexToBase64 = hexToBase64;
    exports.base64ToHex = base64ToHex;


})(exports);
